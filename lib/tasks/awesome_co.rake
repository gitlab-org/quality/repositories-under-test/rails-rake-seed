# frozen_string_literal: true

namespace :db do
  namespace :seed do
    desc 'Seed AwesomeCo for a given Namespace'
    task :awesome_co, [:namespace_id] => :environment do |task, argv|
      namespace = Namespace.find_by_id(argv[:namespace_id])

      puts "Seeding AwesomeCod for #{namespace.name}..."

      begin # seeding
        require Rails.root.join('db', 'seeds', 'awesome_co_1_seed.rb')

        AwesomeCo.seed(namespace.id)
      rescue => e
        # something went wrong with seeding
        raise e
      end

      puts "Seeded"
    end
  end
end
