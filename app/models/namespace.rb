# frozen_string_literal: true

class Namespace < ApplicationRecord
  belongs_to :namespace
  has_many :namespaces

  has_many :projects
end
