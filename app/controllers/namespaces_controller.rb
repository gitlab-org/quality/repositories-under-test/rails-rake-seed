# frozen_string_literal: true

class NamespacesController < ApplicationController
  def index
    @namespaces = Namespace.all
  end

  def show
    @namespace = Namespace.find(params[:id])
  end

  def new

  end

  def create

  end

  # POST /:id/seed
  def seed
    # seed this Namespace with AwesomeCo

    %x[rake db:seed:awesome_co[#{params[:namespace_id]}]] # NOTE huge security risk this way.

    # Rake::Task['db:seed:awesome_co'].invoke(id)
    redirect_to Namespace.find(params[:namespace_id])
  end
end
