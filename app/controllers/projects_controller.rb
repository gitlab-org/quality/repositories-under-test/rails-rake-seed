# frozen_string_literal: true

class ProjectsController < ApplicationController
  def show
    @project = Namespace.find(params[:namespace_id]).projects.find(params[:id])
  end
end
