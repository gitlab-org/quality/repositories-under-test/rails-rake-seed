# Rails Rake Seed Sample Project

This project is a Proof-of-Concept that shows invoking rake tasks from the UI. 

These rake tasks will invoke seed files to seed demo data from AwesomeCo.

## Get Started

```shell
$ bundle install
$ bundle exec rake db:setup # db:migrate if already setup
$ bundle exec rake db:seed # seed two sample namespaces just to get started.
$ bundle exec rails s
```

## Seed a Namespace with AwesomeCo

Get the `id` column of a Namespace.

```shell
$ bundle exec rake 'db:seed:awesome_co[1]'
```

> where `1` is the ID of the Namespace

The Namespace should be seeded with the content in `db/seeds/awesome_co_1_seed.rb`

# Key Players

- Rake file `lib/tasks/awesome_co.rb`; The rake task that is invoked from Rails to populate a given Namespace ID
- Seed file `db/seeds/awesome_co_1_seed.rb`;  Declares what seed data to put in

# Considerations

- The `db/seeds/awesome_co_1_seed.rb` file pulls directly from the `spec/factories`.  We should set up a symlink so we
are not mixing the :production and :test/:development rails environments.
