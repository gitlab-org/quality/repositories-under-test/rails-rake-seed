# frozen_string_literal: true

Rails.application.routes.draw do
  get 'projects/show'
  root to: 'namespaces#index'

  resources :namespaces do
    resources :namespaces

    resources :projects

    post 'seed'
  end
end
