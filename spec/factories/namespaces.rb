# frozen_string_literal: true

FactoryBot.define do
  factory :namespace do
    sequence :name do |n|
      "Namespace #{n}"
    end
  end
end
