# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    sequence :name do |n|
      "Project #{n}"
    end

    namespace { create(:namespace) }
  end
end
