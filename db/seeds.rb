# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# this isn't required in production.  Only for this purpose.  We wouldn't have to touch this file.
# HOWEVER, it *would* for self-managed instances.

n1 = Namespace.create(name: 'Namespace 1')
n2 = Namespace.create(name: 'Namespace 2')
