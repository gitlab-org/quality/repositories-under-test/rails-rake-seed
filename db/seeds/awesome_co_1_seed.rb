# frozen_string_literal: true

include FactoryBot::Syntax::Methods
# Seed v1 AwesomeCod

module AwesomeCo
  module_function

  # Seed a specific namespace
  # @param [Integer] namespace_id the id of the namespace to seed
  def seed(namespace_id)
    # create 3 named projects
    create(:project, namespace_id: namespace_id, name: 'First project')
    create(:project, namespace_id: namespace_id, name: 'Second project')
    create(:project, namespace_id: namespace_id, name: 'Third project')

    # create a named namespace
    create(:namespace, namespace_id: namespace_id, name: 'My Custom Name')
    # create 3 sample namespaces
    # create 2 projects for each sub-namespace
    3.times do
      n = create(:namespace, namespace_id: namespace_id)

      2.times do
        create(:project, namespace: n)
      end
    end
  end
end
