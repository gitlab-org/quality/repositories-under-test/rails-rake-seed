# frozen_string_literal: true

class CreateNamespaces < ActiveRecord::Migration[6.1]
  def change
    create_table :namespaces do |t|
      t.string :name
      t.integer :namespace_id

      t.timestamps
    end
  end
end
